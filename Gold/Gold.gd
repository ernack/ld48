extends Sprite

func _ready():
	pass


func _on_Area2D_body_entered(body):
	if body.is_in_group('player'):
		GScore.take_gold()
		GAudio.coin()
		queue_free()
