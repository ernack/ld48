extends Control


func _ready():
	GScore.connect("on_gold_taken", self, '_on_gold_taken')
	$Bar/LevelLabel.text = 'LVL: ' + str(GLevel.current_level()+1)
	
	_on_gold_taken()
	
func _on_gold_taken():
	$Bar/GoldLabel.text = 'GOLD: ' + str(GScore.gold)

func _process(delta):
	if GScore.is_wagon_found():
		$Bar/Obj0.modulate = Color.green
	if GScore.is_boss_killed():
		$Bar/Obj1.modulate = Color.green
	if GLevel.current_level() == 6:
		$Bar/Obj2.modulate = Color.green
