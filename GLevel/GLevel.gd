extends Node

const LEVEL_MAX = 7
var m_current_level = 0
var m_ready = false
signal on_next_level

func _ready():
	pass

func current_level():
	return m_current_level

func ready(b : bool):
	m_ready = b
	
func next_level():
	if m_current_level < LEVEL_MAX:
		m_current_level += 1
		
		emit_signal("on_next_level")

func next_level_if_ready():
	if m_ready:
		next_level()
		m_ready = false
		return true
	return false
