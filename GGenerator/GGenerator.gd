extends Node

const EMPTY = -1
const WALL = 0
const FLOOR = 1
const MIN_ROOM_W = 8
const MIN_ROOM_H = MIN_ROOM_W
const MIN_ENEMIES = 4
const MAX_ENEMIES = 8
const MIN_GOLD = 2
const MAX_GOLD = 8
var m_rooms = []
var m_safe_spot = null
var m_map_ratio = 5
var m_ladder = null


func _ready():
	pass

class Room:
	var x = 0
	var y = 0
	var w = 0
	var h = 0
	var type = WALL
	
	func _init(x, y, w, h):
		self.x = x
		self.y = y
		self.w = w
		self.h = h
	
func rooms(type = FLOOR):
	var res = []
	for room in m_rooms:
		if room.type == type:
			res.append(room)
	return res
	
func safe_spot():
	return m_safe_spot

func generate(player, node, tilemap : TileMap, level : int):
	if GLevel.current_level() >= GLevel.LEVEL_MAX: return 
	for child in node.get_children():
		child.queue_free()
	
	_generate_level(tilemap, level)
	player.set_position(_random_pos(tilemap, player.size()))
	

	var player_pos = tilemap.world_to_map(player.position())
	player_pos /= 5
	player_pos.x = int(player_pos.x)
	player_pos.y = int(player_pos.y)
	
	var ladder_pos = null
	
	while true:
		if level < GLevel.LEVEL_MAX:
			ladder_pos = tilemap.world_to_map(_generate_ladder(tilemap, node, level))
			ladder_pos /= 5
			ladder_pos.x = int(ladder_pos.x)
			ladder_pos.y = int(ladder_pos.y)
		if _is_reachable(tilemap, player_pos, ladder_pos):
			break
	node.add_child(m_ladder)
	
	_generate_enemies(tilemap, node, level)
	_generate_gold(tilemap, node, level)
	
	if level >= GLevel.LEVEL_MAX - 1:
		_generate_boss(tilemap, node)
		_generate_wagon(tilemap, node)
		
func _get_neighbors(tilemap : TileMap, p):
	var res = []
	
	if tilemap.get_cell(p.x-1, p.y) == FLOOR: res.append(Vector2(p.x-1, p.y))
	if tilemap.get_cell(p.x+1, p.y) == FLOOR: res.append(Vector2(p.x+1, p.y))
	if tilemap.get_cell(p.x, p.y+1) == FLOOR: res.append(Vector2(p.x, p.y+1))
	if tilemap.get_cell(p.x, p.y-1) == FLOOR: res.append(Vector2(p.x, p.y-1))
	
	return res

func _is_in_list(p0, lst):
	for p in lst:
		if p.x == p0.x and p.y == p0.y:
			return true
	return false

func _is_reachable(tilemap : TileMap, p0, p1):
	var openlst = [p0]
	var closelst = []
	
	while not openlst.empty():
		var current = null
		var dist = null
		var idx = null
		
		current = openlst.back()
		openlst.pop_back()
		
		if current != null and current.x == p1.x and current.y == p1.y:
			return true
		
		for neighbor in _get_neighbors(tilemap, current):
			if not neighbor in closelst:
				openlst.push_back(neighbor)
				
		
		closelst.push_back(current)
		
	return false
	
func _generate_gold(tilemap : TileMap, node, level):
	for i in range(0, randi()%(MAX_GOLD * (level + 1))):
		var gold = preload("res://Gold/Gold.tscn").instance()
		node.add_child(gold)
		gold.set_position(_random_pos(tilemap, gold.get_rect().size))
		
			
func _generate_enemies(tilemap : TileMap, node, level):
	for i in range(MIN_ENEMIES, MIN_ENEMIES + randi()%MAX_ENEMIES):
		var enemy = preload("res://Actor/Enemy.tscn").instance()
		node.add_child(enemy)
		enemy.rand_skin()
		enemy.set_position(_random_pos(tilemap, enemy.size()))

func _generate_boss(tilemap, node):
	var enemy = preload("res://Actor/Enemy.tscn").instance()
	node.add_child(enemy)
	
	enemy.set_skin(1, 1)
	enemy.to_boss()
	enemy.set_position(_random_pos(tilemap, enemy.size()))
	
func _generate_wagon(tilemap, node):
	var wagon = preload("res://Wagon/Wagon.tscn").instance()
	node.add_child(wagon)
	wagon.set_position(_random_pos(tilemap, wagon.get_rect().size))	
	
func _generate_ladder(tilemap, node, level):
	m_ladder = preload("res://Ladder/Ladder.tscn").instance()
	
	if not m_rooms.empty():
		var room = m_rooms[randi()%m_rooms.size()]
		
		m_ladder.position = _random_pos(tilemap, m_ladder.get_rect().size)
		m_ladder.position -= m_ladder.get_rect().size/2
		
		
		return m_ladder.position
	return null
	
func _random_pos(tilemap : TileMap, size):
	var player = get_tree().get_nodes_in_group('player')
	var max_itr = 1024;
	var itr = 0
	
	if not player.empty():
		var player_pos = tilemap.world_to_map(player[0].position())
		player_pos /= 5
		player_pos.x = int(player_pos.x)
		player_pos.y = int(player_pos.y)
		
		while true:
			var room = m_rooms[randi()%m_rooms.size()]

			var x = room.x + room.w/2
			var y = room.y + room.h/2
			
			var pos = 16 * m_map_ratio * Vector2(x , y)
			
			var next_pos = pos
			next_pos = tilemap.world_to_map(next_pos)
			next_pos /= 5
			next_pos.x = int(next_pos.x)
			next_pos.y = int(next_pos.y)
			
			if _is_reachable(tilemap, player_pos, next_pos) or itr >= max_itr:
				return pos
			itr += 1
		return null
	
func _generate_level(tilemap : TileMap, level : int):
	_clear_tilemap(tilemap)
	
	var depth = 8
	var x = 0
	var y = 0
	var w = 32
	var h = w
	
	_draw_rect(tilemap, x-1, y-1, w+2, h+2, WALL)
	_binary_gen(tilemap, x, y, w, h, depth)

	var o = 2
	x = x + o
	y = y + o
	w = w - 2 * o
	h = h - 2 * o
	
	for room in m_rooms:
		_draw_rect(tilemap, room.x, room.y, room.w, room.h, FLOOR)
		
		var spot = Vector2(room.x + room.w/2, room.y + room.h/2)
		if spot.x > x and spot.x < x + w and spot.y > y and spot.y < y + w:
			m_safe_spot = 16 * m_map_ratio * spot
		
func _binary_gen(tilemap : TileMap, x, y, w, h, depth):
	if depth < 0 : return
	
	if depth % 2 == 0:
		var ysplit = floor(rand_range(1, w))
		_binary_gen(tilemap, x, y, ysplit, h, depth - 1)
		if w - ysplit > 0:
			_binary_gen(tilemap, x + ysplit, y,  w - ysplit, h, depth - 1)
	else:
		var xsplit = floor(rand_range(1, h))
		_binary_gen(tilemap, x, y, w, xsplit, depth - 1)
		if h - xsplit > 0:
			_binary_gen(tilemap, x, y + xsplit, w,  h - xsplit, depth - 1)
	
	var room = Room.new(x, y, w, h)
	assert(w > 0)
	assert(h > 0)
	
	var thres = 8
	
	if room.w > thres or room.h > thres:
		room.type = WALL
	else:
		room.type = FLOOR

		m_rooms.append(room)
		
func _clear_tilemap(tilemap : TileMap):
	tilemap.clear()
	m_rooms.clear()
	
func _draw_room(tilemap : TileMap, x, y, w, h):
	_draw_rect(tilemap, x, y, w, h)
	_draw_rect(tilemap, x + 1, y + 1, w - 2, h - 2, FLOOR)
	
func _draw_rect(tilemap : TileMap, x, y, w, h, tile_id = WALL):
	for i in range(y, y + h):
		for j in range(x, x + w):
			tilemap.set_cell(j, i, tile_id)

func _clear_rect(tilemap : TileMap, x, y, w, h):
	for i in range(y, y + h):
		for j in range(x, x + w):
			tilemap.set_cell(j, i, -1)
