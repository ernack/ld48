extends Sprite


func _ready():
	pass


func _on_Area2D_body_entered(body):
	if body.is_in_group('player'):
		GLevel.ready(true)


func _on_Area2D_body_exited(body):
	if body.is_in_group('player'):
		GLevel.ready(false)
