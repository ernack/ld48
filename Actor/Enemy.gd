extends Node

var m_state = null

func _ready():
	$Actor/Sprite.texture = preload("res://Actor/spritesheet.png")
	$Actor/Sprite.region_enabled = true
	$Actor/Sprite.region_rect = Rect2(32, 0, 16, 16)
	
	m_state = $IdleState
	m_state.set_enemy(self)
	
func actor():
	return $Actor
	
func size():
	return $Actor/Sprite.get_rect().size
	
func set_skin(x, y):
	$Actor/Sprite.region_rect = Rect2(16 * x, 16 * y, 16, 16)
	
func rand_skin():
	set_skin(1 + randi()%3, 0)
	
func to_boss():
	$Actor.m_is_boss = true

func _process(delta):
	if $Actor != null:
		$LightOccluder2D.global_position = $Actor/Sprite.global_position
	elif $LightOccluder2D != null:
		$LightOccluder2D.queue_free()
	
	if m_state != null:
		m_state.process(delta)
