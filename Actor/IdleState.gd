extends Node

var m_enemy = null
var m_player = null

func _ready():
	pass
	
func set_enemy(enemy):
	m_enemy = enemy
	m_enemy.actor().wander()
	
func process(delta):
	m_player = get_tree().get_nodes_in_group('player')
	
	if not m_player.empty():
		var target = m_player[0].position() - m_enemy.actor().position()
		if target.length() < 256:
			m_enemy.m_state = get_parent().get_node("FightState")
			m_enemy.m_state.set_enemy(m_enemy)
			queue_free()

