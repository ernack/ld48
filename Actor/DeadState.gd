extends Node

var m_actor = null

func _ready():
	pass
	
func set_actor(actor):
	m_actor = actor
	if m_actor.is_boss():
		GScore.boss_killed()
		
func process(_delta):
	m_actor.queue_free()
