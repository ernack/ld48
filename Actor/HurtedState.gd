extends Node

var m_actor = null
var m_timer = 0.0
var m_time = 0.20

func _ready():
	pass
	
func set_actor(actor):
	m_actor = actor
	m_actor.modulate(Color(1, 0, 0))

func process(delta):
	assert(m_actor)
	var ratio = sin(m_timer/m_time)
	m_actor.modulate(Color(1, ratio, ratio))
	
	if m_timer >= m_time:
		m_actor.modulate(Color(1, 1, 1))
		m_timer = 0
		if m_actor.m_life <= 0:
			m_actor.set_state(m_actor.get_node("DeadState"))
			if m_actor in get_tree().get_nodes_in_group('player'):
				GAudio.defeat()
				GAudio.ow()
			else:
				GAudio.monster()
			for sandbox in get_tree().get_nodes_in_group('sandbox'):
				var gold = preload("res://Gold/Gold.tscn").instance()
				gold.global_position = m_actor.position()
				sandbox.get_node('Level').add_child(gold)
			
		else:
			m_actor.set_state(m_actor.get_node("AliveState"))
	m_timer += delta
	
