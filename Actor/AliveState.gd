extends Node

var m_actor = null
var m_force = Vector2()

func _ready():
	pass
	
func set_actor(actor):
	m_actor = actor
	
func process(delta):
	assert(m_actor)

	if m_actor.m_sep != null:
		m_actor.m_velocity += 0.8 * m_actor.m_sep.force(delta)
	
	if m_actor.m_steering != null:
		var acceleration = m_actor.m_steering.force(delta)
		m_actor.m_velocity += 0.2 * m_actor.m_steering.force(delta)
		
	if m_actor.m_velocity.length_squared() > m_actor.m_speed:
			m_actor.m_velocity = m_actor.m_velocity.normalized() * m_actor.m_speed
			
	m_actor.move_and_collide(m_actor.m_velocity * delta)
	
