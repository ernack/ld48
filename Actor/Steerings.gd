extends Node

class Seek:
	var m_pos = null
	var m_actor = null
	
	func _init(actor, pos):
		m_actor = actor
		m_pos = pos
		
	func set_position(pos):
		m_pos = pos
		
	func force(_delta):
		var desired = m_pos - m_actor.position()
		var result = desired - m_actor.m_velocity
			
		return result

class Wander:
	var m_actor = null
	var m_dist = 256.0
	var m_radius = 128.0

	var m_time = 0.5
	var m_timer = m_time
	
	var m_result = Vector2()
	
	func _init(actor):
		m_actor = actor
		
	func force(delta):
		
		if m_timer > m_time:
			var vel = m_actor.m_velocity
			var dir = Vector2(1, 0)
			
			if vel.length_squared() != 0:
				dir = vel.normalized()
				
			var angle = rand_range(0.0, 2 * PI)
			var desired = dir * m_dist + m_radius * Vector2(cos(angle), sin(angle))
			
			m_result = desired - vel
				
			m_timer = 0.0
		m_timer += delta
		
		return m_result

class Separation:
	var m_actor = null
	var m_others = []
	
	func _init(actor, others):
		m_actor = actor
		m_others = others
		
	func force(delta):
		var vec = []
		
		for actor in m_others:
			if actor == null or actor == m_actor: continue
			var target = m_actor.position() - actor.position()
			if target.length() < 128.0:
				vec.append(target)
			
		var desired = Vector2()
		
		if vec.empty():
			return Vector2()
			
		for v in vec:
			desired += v
		desired /= vec.size()
		
		var result = desired 
		
		return result.normalized() * m_actor.SPEED_RUN
		
