extends Node

var m_enemy = null
var m_range = 120
var m_timer = 0.0
var m_time = 1.0

func _ready():
	pass
	
func set_enemy(enemy):
	m_enemy = enemy
	m_enemy.actor().seek(Vector2())
	
func process(delta):
	assert(m_enemy)
	var player = get_tree().get_nodes_in_group('player')
	if player.empty(): return
	player = player[0]
	
	assert(player)
	if m_enemy.actor() != null:
		m_enemy.actor().seek(player.position())
		
		var target = player.position() - m_enemy.actor().position()
		if target.length() < m_range:
			if m_timer >= m_time:
				player.hurt()
				GAudio.hit()
				m_timer = 0.0
			
	m_timer += delta
	
