extends Node

var m_last_pos = null

func _ready():
	$Actor.add_to_group('player')
	$Actor.run()
	$Actor.m_sep = null
	
func set_position(pos : Vector2):
	$Actor.global_position = pos
	
func size():
	return $Actor/Sprite.get_rect().size
	
func is_dead():
	return get_node_or_null("Actor") == null
	
func position():
	if get_node_or_null("Actor") != null:
		m_last_pos = $Actor.position()
		return m_last_pos
	return m_last_pos

func _process(delta):
	
	if Input.is_action_just_pressed("player_attack"):
		if get_node_or_null("Actor") != null:
			$Actor.attack()
	if Input.is_action_pressed("player_action"):
		if GLevel.next_level_if_ready():
			GAudio.ladder()
		
	self.__process_move(delta)

func __process_move(_delta):
	var dir = Vector2()
	
	if Input.is_action_pressed("player_up"):
		dir.y -= 1
	if Input.is_action_pressed("player_down"):
		dir.y += 1
	if Input.is_action_pressed("player_left"):
		dir.x -= 1
	if Input.is_action_pressed("player_right"):
		dir.x += 1
		
	if dir.length_squared() != 0:
		dir = dir.normalized()
	if get_node_or_null("Actor") != null:
		$Actor.move(dir * Vector2(1, 1) * $Actor.speed())
