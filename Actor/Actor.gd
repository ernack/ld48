extends KinematicBody2D

const SPEED_RUN = 256
const SPEED_WALK = 128

var m_speed = SPEED_WALK
var m_acceleration = Vector2(0, 0)
var m_velocity = Vector2(0, 0)

var m_range = 142
var m_max_life = 4
var m_life = m_max_life
var m_state = null

var m_steering = null
var m_sep = null
var m_wander = null

var m_is_boss = false

func _ready():
	self.add_to_group('actors')
	$Node2D/ProgressBar.max_value = m_max_life
	set_state($AliveState)
	m_sep = $Steerings.Separation.new(self, get_tree().get_nodes_in_group('actors'))
	
func wander():
	if not m_steering is $Steerings.Wander:
		m_steering = $Steerings.Wander.new(self)
	
func seek(pos):
	m_steering = $Steerings.Seek.new(self, pos)
	
func stop():
	m_steering = null

func _process(delta):
	if m_state:
		m_state.process(delta)
	
	$Node2D/ProgressBar.value = m_life
	
func is_boss():
	return m_is_boss
func walk():
	m_speed = SPEED_WALK
	
func run():
	m_speed = SPEED_RUN
	
func position():
	return self.global_position
	
func speed():
	return m_speed
	
func move(delta_pos : Vector2):
	if m_state == $AliveState:
		m_velocity = delta_pos

func modulate(color):
	$Sprite.modulate = color

func attack():
	for actor in get_tree().get_nodes_in_group('actors'):
		if actor != self and actor.m_state == actor.get_node('AliveState'):
			var dist2 = (actor.global_position - self.global_position).length_squared()
			
			if dist2 < m_range * m_range:
				actor.hurt()
				GAudio.hit()
				
			
func hurt():
	set_state($HurtedState)
	m_life -= 1
	
func set_state(state):
	m_state = state
	m_state.set_actor(self)
