extends Node
onready var m_player = AudioStreamPlayer.new()
onready var m_music = AudioStreamPlayer.new()
onready var m_coin = preload("res://Sounds/coin.wav")
onready var m_ladder = preload("res://Sounds/ladder.wav")
onready var m_defeat = preload("res://Sounds/defeat.wav")
onready var m_hit = preload("res://Sounds/hit.wav")
onready var m_ow = preload("res://Sounds/ow.wav")
onready var m_main = preload("res://Sounds/the_mine.wav")
onready var m_monster = preload("res://Sounds/monsterdie.wav")
onready var m_current = 0

var m_tracks = [
	preload("res://Sounds/music/1_Kickds.wav"),
	preload("res://Sounds/music/2_Organ.wav"),
	preload("res://Sounds/music/3_Harp.wav"),
	preload("res://Sounds/music/4_Timpani.wav"),
	preload("res://Sounds/music/5_Strings.wav"),
	preload("res://Sounds/music/6_Guitar.wav")
]

var m_track_players = []

func _ready():
	add_child(m_player)
	add_child(m_music)
		
	for stream in m_tracks:
		m_track_players.push_back(AudioStreamPlayer.new())
		
		m_track_players.back().stream = stream
		add_child(m_track_players.back())
	
func music_next():
	if m_current > m_track_players.size(): return
	
	for player in m_track_players:
		player.stop()
		
	for i in range(0, max(1, m_current)):
		m_track_players[i].play()
	
	m_current += 1

func stop_music():
	m_music.stop()
	for player in m_track_players:
		player.stop()
func main():
	m_music.stream = m_main
	m_music.play()
	
func monster():
	m_player.stream = m_monster
	m_player.play()

func ow():
	m_player.stream = m_ow
	m_player.play()
	
func hit():
	m_player.stream = m_hit
	m_player.play()
	
func coin():
	m_player.stream = m_coin
	m_player.play()
	
func ladder():
	m_player.stream = m_ladder
	m_player.play()
	
func defeat():
	m_music.stream = m_defeat
	m_music.play()
