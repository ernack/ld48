extends Node

var m_sandbox = null
var m_timer = 0.0
var m_time = 1.0
var m_color_rect : ColorRect
var m_path = null

func set_path(path):
	m_path = path

func set_sandbox(sandbox):
	m_sandbox = sandbox
	m_color_rect = ColorRect.new()
	m_color_rect.color = Color.black
	m_color_rect.color.a = 0.0
	m_color_rect.rect_min_size = get_viewport().size
	m_color_rect.set_position(Vector2())
	
	var canvas = CanvasLayer.new()
	canvas.add_child(m_color_rect)
	add_child(canvas)
	
func _ready():
	pass

func process(delta):
	assert(m_sandbox.m_player)
	var ratio = m_timer / m_time
	
	m_color_rect.color.a = ratio
	if m_timer >= m_time:
		m_sandbox.queue_free()
		get_tree().change_scene(m_path)
		m_timer = 0.0
		
	var zoom_factor_start = 1
	var zoom_factor_end = 0.2
	var zoom_factor = (1 - ratio) * zoom_factor_start + (ratio) * zoom_factor_end
	m_sandbox.get_cam().zoom = Vector2(zoom_factor, zoom_factor)	
	m_timer += delta
