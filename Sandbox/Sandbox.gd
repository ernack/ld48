extends Node2D

var m_player = null
onready var m_state = $GameState

func _ready():
	randomize()

	m_player = $Player
	m_state.set_sandbox(self)
	
	GScore.connect("on_victory", self, '_on_victory')
	GLevel.connect("on_next_level", self, '_on_next_level')
	
	GGenerator.generate($Player, $Level, $Tilemap, GLevel.current_level())
	var rooms = GGenerator.rooms()
	
func get_cam():
	return $Camera2D
	
func _process(delta):
	if Input.is_action_just_pressed("ui_home"):
		GLevel.next_level()
	if m_state:
		m_state.process(delta)
	$Light2D.global_position = m_player.position()
	var t = float(GLevel.current_level())/float(GLevel.LEVEL_MAX)
	if t <= 1.0:
		var x = (1.0 - t) * (1.0 - t)
		$CanvasModulate.color = Color(x, x, x)
		
func _on_victory():
	m_state = $TransitionState
	m_state.set_sandbox(self)
	m_state.set_path('res://EndGame/EndGame.tscn')
	
func _on_next_level():
	if GLevel.current_level() < GLevel.LEVEL_MAX:
		GAudio.music_next()
		m_state = $TransitionState
		m_state.set_sandbox(self)
		m_state.set_path('res://Sandbox/Sandbox.tscn')

