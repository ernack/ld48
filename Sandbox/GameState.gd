extends Node

var m_sandbox = null

func set_sandbox(sandbox):
	m_sandbox = sandbox
	
func _ready():
	pass

func process(_delta):
	assert(m_sandbox.m_player)
	m_sandbox.get_node('Camera2D').position = m_sandbox.m_player.position()
	
	if m_sandbox.m_player.is_dead():
		m_sandbox.m_state = m_sandbox.get_node('TransitionState')
		m_sandbox.m_state.set_sandbox(m_sandbox)
		m_sandbox.m_state.set_path('res://EndGame/EndGame.tscn')
