extends Node

var success = false
var wagon = false
var boss = false

var gold = 0

signal on_gold_taken
signal on_victory

func _ready():
	pass

func score():
	return gold
	
func is_wagon_found():
	return wagon
	
func is_boss_killed():
	return boss
	
func take_gold():
	gold += 1
	emit_signal("on_gold_taken")
	
func wagon_found():
	wagon = true
	print('wagon found')
	check()

func boss_killed():
	boss = true
	print('bosskilled')
	check()

func check():
	if boss and wagon:
		success = true
		emit_signal("on_victory")
