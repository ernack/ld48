extends Control

var m_measure = -1
var m_progression = 0

func _ready():
	GAudio.main()
	$Timer.wait_time = 1.0/(140.0/60.0)
	

func _process(delta):
	if Input.is_action_pressed("player_action"):
		GAudio.stop_music()
		GAudio.music_next()
		get_tree().change_scene("res://Sandbox/Sandbox.tscn")
	
	$Sprite.global_position += delta * Vector2(-64, 0)
	$Sprite.rotate(PI/8.0 * delta)
	
	$Sprite2.global_position += delta * Vector2(64, 0)
	$Sprite2.rotate(PI/8.0 * delta)
	

var m_new_color = Color.white

func _on_Timer_timeout():
	m_measure += 1
	if m_measure >= 4:
		m_measure = 0
		m_progression += 1
	
	if m_progression >= 3 and (m_measure == 0 or m_measure == 2):
		
		if m_measure == 0:
			$Label.modulate = m_new_color
			m_new_color = Color(rand_range(0.0, 1.0), rand_range(0.0, 1.0), rand_range(0.0, 1.0))
			$Label2.modulate = m_new_color
			$Label2.get_font("font").size = 90
			$Label.get_font("font").size = 64
		else:
			$Label.modulate = m_new_color
			m_new_color = Color(rand_range(0.0, 1.0), rand_range(0.0, 1.0), rand_range(0.0, 1.0))
			$Label2.modulate = m_new_color
			
			$Label2.get_font("font").size = 64
			$Label.get_font("font").size = 90
